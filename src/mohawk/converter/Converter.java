package mohawk.converter;

import java.util.logging.Logger;

public class Converter {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        ConverterInstance inst = new ConverterInstance();

        inst.run(args);
    }

}
