package mohawk.converter.testing;

import static org.junit.Assert.*;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.antlr.v4.runtime.BaseErrorListener;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import mohawk.global.helper.FileExtensions;
import mohawk.global.helper.ParserHelper;
import mohawk.global.parser.BooleanErrorListener;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.*;

/** Run these JUnit tests before adding any branch to master
 * 
 * @author Jonathan Shahen */

public class ConverterRegressionTests {
    public final Logger logger = Logger.getLogger("mohawk");

    public String folderbase = "data/regressiontests/";
    public BooleanErrorListener error = new BooleanErrorListener();

    @Before
    public void printTestHeading() {
        System.out.println(StringUtils.repeat("-", 80));
        System.out.println(StringUtils.repeat("-", 80));
    }

    @Test
    public void runPositive1() throws IOException {
        System.out.println("Running Test: runPositive1\n\n");
        String file = "positive1.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        // Check for Parsing errors
        assertFalse("Errors were found!", error.errorFound);

        // Correct number of rules per block
        assertEquals("Not the correct number of Rule in CanAssign Block", 2, m.canAssign.size());
        assertEquals("Not the correct number of Rule in CanRevoke Block", 0, m.canRevoke.size());
        assertEquals("Not the correct number of Rule in CanEnable Block", 0, m.canEnable.size());
        assertEquals("Not the correct number of Rule in CanDisable Block", 0, m.canDisable.size());

        // Correct number of roles and timeslots
        assertEquals("Incorrect number of Roles", new Integer(4), m.roleHelper.numberOfRoles());
        // t0, t1, t2, t5
        assertEquals("Incorrect number of Timeslots", new Integer(4), m.timeIntervalHelper.size());
        // t0, t1, t2, t5
        assertEquals("Incorrect max Timeslot", new Integer(5), m.timeIntervalHelper.maxTimeSlot);
    }

    @Test
    public void runPositive2() throws IOException {
        System.out.println("Running Test: runPositive2\n\n");
        String file = "positive2.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        // Check for Parsing errors
        assertFalse("Errors were found!", error.errorFound);

        // Correct number of rules per block
        assertEquals("Not the correct number of Rule in CanAssign Block", 2, m.canAssign.size());
        assertEquals("Not the correct number of Rule in CanRevoke Block", 0, m.canRevoke.size());
        assertEquals("Not the correct number of Rule in CanEnable Block", 2, m.canEnable.size());
        assertEquals("Not the correct number of Rule in CanDisable Block", 2, m.canDisable.size());

        // Correct number of roles and timeslots
        assertEquals("Incorrect number of Roles", new Integer(5), m.roleHelper.numberOfRoles()); //
        // t0, t1, t2, t5
        assertEquals("Incorrect number of Timeslots", new Integer(4), m.timeIntervalHelper.size());
        // t0, t1, t2, t5
        assertEquals("Incorrect max Timeslot", new Integer(5), m.timeIntervalHelper.maxTimeSlot);
    }

    @Test
    public void runPositive3() throws IOException {
        System.out.println("Running Test: runPositive3\n\n");
        String file = "positive3.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        // Check for Parsing errors
        assertFalse("Errors were found!", error.errorFound);

        // Correct number of rules per block
        assertEquals("Not the correct number of Rule in CanAssign Block", 3, m.canAssign.size());
        assertEquals("Not the correct number of Rule in CanRevoke Block", 0, m.canRevoke.size());
        assertEquals("Not the correct number of Rule in CanEnable Block", 2, m.canEnable.size());
        assertEquals("Not the correct number of Rule in CanDisable Block", 2, m.canDisable.size());

        // Correct number of roles and timeslots
        assertEquals("Incorrect number of Roles", new Integer(6), m.roleHelper.numberOfRoles()); //
        // t0, t1, t2, t5
        assertEquals("Incorrect number of Timeslots", new Integer(4), m.timeIntervalHelper.size());
        // t0, t1, t2, t5
        assertEquals("Incorrect max Timeslot", new Integer(5), m.timeIntervalHelper.maxTimeSlot);
    }

    @Test
    public void runPositive4() throws IOException {
        System.out.println("Running Test: runPositive4\n\n");
        String file = "positive4.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        // Check for Parsing errors
        assertFalse("Errors were found!", error.errorFound);
        ArrayList<Rule> rules = m.canAssign.getRules();

        assertEquals("Time interval not the same for full style", new TimeInterval(0, 5),
                rules.get(0)._adminTimeInterval);
        assertEquals("Time interval not the same for the half style", new TimeInterval(0, 0),
                rules.get(1)._adminTimeInterval);
    }

    @Test
    public void testTimeInterval() {
        System.out.println("Running Test: testTimeInterval\n\n");
        TimeInterval t1 = new TimeInterval(1, 5);
        TimeInterval t2 = new TimeInterval(1, 5);
        TimeInterval t3 = new TimeInterval(2, 3);
        TimeInterval t4 = new TimeInterval(3, 3);
        TimeInterval t5 = new TimeInterval(4, 6);
        TimeInterval t6 = new TimeInterval(0, 3);
        TimeInterval t7 = new TimeInterval(6, 7);
        TimeInterval t8 = new TimeInterval(0, 0);

        assertTrue("isEqual", t1.equals(t2));
        assertTrue("isOverlapping same", t1.isOverlapping(t2));
        assertTrue("isOverlapping within", t1.isOverlapping(t3));
        assertTrue("isOverlapping timeslot", t1.isOverlapping(t4));
        assertTrue("isOverlapping in and after", t1.isOverlapping(t5));
        assertTrue("isOverlapping before and in", t1.isOverlapping(t6));
        assertFalse("isOverlapping outside after", t1.isOverlapping(t7));
        assertFalse("isOverlapping outside before timeslot", t1.isOverlapping(t8));

    }

    @Test
    public void checkFileExtension() {
        System.out.println("Checking to make sure FileExtensions is included "
                + "(This is mainly for developing and including the Mohawk-T Globals project)");
        try {
            FileExtensions fe = new FileExtensions();

            assertNotNull("FileExtensions was not included!", fe);
        } catch (Exception e) {
            assertFalse("An exception was raised when trying to create the object FileExtension!\n"
                    + "Make sure you have included the Mohawk-T Globals project when building the jar", true);
        }
    }

    @Test
    public void runNegative0() throws IOException {
        System.out.println("Running Test: runNegative0\n\n");
        String str = "";

        error.errorFound = false;
        runParser(new ByteArrayInputStream(str.getBytes()), error);

        // Check for Parsing errors
        assertTrue("The empty string passed the parser!", error.errorFound);
    }

    private MohawkTARBACParser loadfile(String file) throws IOException {
        FileInputStream fis = new FileInputStream(folderbase + file);

        return runParser(fis, error);
    }

    public static MohawkTARBACParser runParser(InputStream is, BaseErrorListener errorListener) throws IOException {
        return ParserHelper.runParser(is, errorListener, true);
    }
}
