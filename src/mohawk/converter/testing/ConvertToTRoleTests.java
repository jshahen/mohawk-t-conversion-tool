package mohawk.converter.testing;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.*;

import org.junit.Before;
import org.junit.Test;

import mohawk.global.convert.tred.ConvertToTRole;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.parser.BooleanErrorListener;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.MohawkT;
import mohawk.global.timing.MohawkTiming;

public class ConvertToTRoleTests {
    public static final Logger logger = Logger.getLogger("mohawk");

    public MohawkTiming timing = new MohawkTiming();
    public String folderbase = "data/regressiontests/";
    public BooleanErrorListener error = new BooleanErrorListener();

    @Before
    public void setup() {
        System.out.println("sadasdasdasdsadsadsaasd");
        logger.setLevel(Level.FINE);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        consoleHandler.setLevel(Level.FINE);
        logger.addHandler(consoleHandler);
    }

    @Test
    public void printOut() throws IOException {
        String file = "positive1.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        ConvertToTRole converter = new ConvertToTRole(timing);

        String convertedStr = converter.convert(m, null, false);

        System.out.println("Printing Out: " + file);
        System.out.println(convertedStr + "\n\n");

    }

    private MohawkTARBACParser loadfile(String file) throws IOException {
        FileInputStream fis = new FileInputStream(folderbase + file);

        return ConverterRegressionTests.runParser(fis, error);
    }

}
