package mohawk.converter.testing;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.*;

import org.junit.Before;
import org.junit.Test;

import mohawk.global.convert.asasptime.ConvertToASAPTimeNSA;
import mohawk.global.convert.asasptime.ConvertToASAPTimeSA;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.parser.BooleanErrorListener;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.MohawkT;
import mohawk.global.timing.MohawkTiming;

public class ConvertToASAPTimeTests {
    public static final Logger logger = Logger.getLogger("mohawk");

    public MohawkTiming timing = new MohawkTiming();
    public String folderbase = "data/regressiontests/";
    public BooleanErrorListener error = new BooleanErrorListener();

    @Before
    public void setup() {
        System.out.println("sadasdasdasdsadsadsaasd");
        logger.setLevel(Level.FINE);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        consoleHandler.setLevel(Level.FINE);
        logger.addHandler(consoleHandler);
    }

    @Test
    public void printOutNSA() throws IOException {
        String file = "positive1.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        ConvertToASAPTimeNSA converter = new ConvertToASAPTimeNSA(timing);

        String convertedStr = converter.convert(m, null, false);

        System.out.println("Printing Out: " + file);
        System.out.println(convertedStr + "\n\n");

    }

    @Test
    public void printOutSA() throws IOException {
        String file = "positive1.spec";

        error.errorFound = false;
        MohawkTARBACParser parser = loadfile(file);
        MohawkT m = parser.mohawkT;

        ConvertToASAPTimeSA converter = new ConvertToASAPTimeSA(timing);

        String convertedStr = converter.convert(m, null, false);

        System.out.println("Printing Out: " + file);
        System.out.println(convertedStr + "\n\n");

    }

    private MohawkTARBACParser loadfile(String file) throws IOException {
        FileInputStream fis = new FileInputStream(folderbase + file);

        return ConverterRegressionTests.runParser(fis, error);
    }

}
