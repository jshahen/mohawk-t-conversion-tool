package mohawk.converter;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

import mohawk.global.helper.FileExtensions;

public class ConverterCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static FileExtensions fileExt = new FileExtensions();
    public static String previousCommandFilename = "ConverterCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        ConverterInstance inst = new ConverterInstance();
        ArrayList<String> argv = new ArrayList<String>();
        String cmd = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!exit' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            cmd = user_input.next();
            fullCommand.append(cmd + " ");

            if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                System.out.println("Starting: " + cmd);
                quotedStr = cmd.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                System.out.println("Ending: " + cmd);
                argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + cmd;
                continue;
            }

            if (cmd.equals("!exit")) {
                break;
            }

            if (cmd.equals("!p")) {
                argv.clear();
                argv.addAll(Arrays.asList(previousCmd.split(" ")));
                break;
            }

            argv.add(cmd);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        try {
            if (!cmd.equals("!p")) {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString());
                fw.close();
            }
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
        }

        inst.run(argv.toArray(new String[1]));
    }

    public static void printCommonCommands() {

        System.out.println("\n\n--- Common Commands ---");
        System.out.println(
                OptionString.TO_ALL.c() + OptionString.SPECFILE.c("data/Mohawk/mixednocr/test04.mohawk.mohawk.T")
                        + OptionString.LOGLEVEL.c() + "debug !exit");
        System.out.println(OptionString.TO_ALL.c() + OptionString.SPECFILE.c() + "data/regressiontests/positive2.spec "
                + OptionString.LOGLEVEL.c() + "debug !exit");
        System.out.println(OptionString.TO_ALL.c() + OptionString.SPECFILE.c() + "data/regressiontests/positive3.spec "
                + OptionString.LOGLEVEL.c() + "debug !exit");
        System.out.println(OptionString.TO_ALL.c() + OptionString.SPECFILE.c() + "data/regressiontests/positive4.spec "
                + OptionString.LOGLEVEL.c() + "debug !exit");
        System.out.println(
                OptionString.TO_ALL.c() + OptionString.SPECFILE.c("data/Mohawk/positive/test01.mohawk.mohawk.T")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println("");

        System.out.println(OptionString.TO_ALL.c() + OptionString.SPECFILE.c("data/Mohawk/positive/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + OptionString.SPECEXT.c(fileExt.Mohawk_T)
                + "!exit");
        System.out.println(OptionString.TO_ASAPTIME_NSA.c() + OptionString.SPECFILE.c("data/reduction/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + OptionString.SPECEXT.c(fileExt.Mohawk_T)
                + "!exit");

        System.out.println("");

        System.out.println(OptionString.TO_ALL.c() + OptionString.SPECFILE.c("data/tests/randomTest.mohawk.T")
                + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(fileExt.Mohawk_T) + "!exit");
        System.out.println(OptionString.TO_ALL.c() + OptionString.SPECFILE.c("data/TestsuiteC") + OptionString.BULK.c()
                + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(fileExt.Mohawk_T) + "!exit");
        System.out.println("");
        System.out.println(
                OptionString.TO_MOHAWK.c() + OptionString.SPECFILE.c("data/TestsuiteC/split1") + OptionString.BULK.c()
                        + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(fileExt.Mohawk_T) + "!exit");

        System.out.println("");
        System.out.println(OptionString.TO_TROLE.c() + OptionString.SPECFILE.c("data/Mahesh/easy/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");
        System.out.println(OptionString.TO_MOHAWK.c() + OptionString.SPECFILE.c("data/Mahesh/easy/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");
        System.out.println(OptionString.TO_ASAPTIME_SA.c() + OptionString.SPECFILE.c("data/Mahesh/easy/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");
        System.out.println(OptionString.TO_ASAPTIME_NSA.c() + OptionString.SPECFILE.c("data/Mahesh/easy/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");

        System.out.println(OptionString.TO_TROLE.c() + OptionString.SPECFILE.c("data/Mahesh/medium/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");
        System.out.println(OptionString.TO_MOHAWK.c() + OptionString.TO_ASAPTIME_SA.c()
                + OptionString.TO_ASAPTIME_NSA.c() + OptionString.SPECFILE.c("data/Mahesh/medium/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");

        System.out.println(OptionString.TO_TROLE.c() + OptionString.SPECFILE.c("data/Mahesh/hard/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");
        System.out.println(OptionString.TO_MOHAWK.c() + OptionString.TO_ASAPTIME_SA.c()
                + OptionString.TO_ASAPTIME_NSA.c() + OptionString.SPECFILE.c("data/Mahesh/hard/")
                + OptionString.BULK.c() + OptionString.LOGLEVEL.c("debug") + OptionString.SPECEXT.c(".spec") + "!exit");

        System.out.println("");
        System.out.println(OptionString.TO_MOHAWK.c() + OptionString.SPECFILE.c("data/bug/") + OptionString.BULK.c()
                + OptionString.SPECEXT.c(".mohawk.T") + OptionString.LOGLEVEL.c("quiet") + "!exit");
        System.out.println(
                OptionString.TO_MOHAWK.c() + OptionString.SPECFILE.c("data/bug/isolation/") + OptionString.BULK.c()
                        + OptionString.SPECEXT.c(".mohawk.T") + OptionString.LOGLEVEL.c("quiet") + "!exit");
        System.out.println(OptionString.TO_MOHAWK.c() + OptionString.SPECFILE.c("data/bug/10-CA-role3.mohawk.T")
                + OptionString.LOGLEVEL.c("quiet") + "!exit");
        System.out.println(OptionString.TO_MOHAWK.c() + OptionString.SPECFILE.c("data/bug/11-CA-no-role3.mohawk.T")
                + OptionString.LOGLEVEL.c("quiet") + "!exit");
        System.out.println("");

        System.out.println(OptionString.TO_NUSMV.c() + OptionString.SPECFILE.c("data/nusmv/test01.mohawkt") + "!exit");
        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command ('!p' to use the previous command): " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
