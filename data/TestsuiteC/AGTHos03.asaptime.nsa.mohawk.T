/* Generated On        : 2015/02/07 20:24:43.407
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 15
 * Number of Timeslots : 10
 * Number of Rules     : 267
 * 
 * Roles     : |Size=15| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15]
 * Timeslots : |Size=10| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t3, [role3]

Expected: UNKNOWN

/* 
 * Number of Rules       : 117
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 82
 * Truly Startable Rules : 20
 */
CanAssign {
    /*   1 */  <role1, t4, role9 & role11, t4, role15>
    /*   2 */  <role1, t2, role9 & role11, t10, role15>
    /*   3 */  <role1, t8, role9 & role11, t5, role15>
    /*   4 */  <role3, t7, TRUE, t10, role14>
    /*   5 */  <role3, t6, TRUE, t4, role14>
    /*   6 */  <TRUE, t3, TRUE, t5, role14>
    /*   7 */  <TRUE, t9, TRUE, t7, role14>
    /*   8 */  <role3, t2, TRUE, t8, role14>
    /*   9 */  <role3, t4, TRUE, t6, role14>
    /*  10 */  <role3, t8, TRUE, t2, role14>
    /*  11 */  <role3, t5, TRUE, t9, role14>
    /*  12 */  <role3, t1, TRUE, t3, role14>
    /*  13 */  <TRUE, t10, TRUE, t1, role14>
    /*  14 */  <role5, t7, TRUE, t8, role4>
    /*  15 */  <role5, t8, TRUE, t2, role4>
    /*  16 */  <role5, t10, TRUE, t4, role4>
    /*  17 */  <role5, t1, TRUE, t8, role6>
    /*  18 */  <role5, t4, TRUE, t8, role8>
    /*  19 */  <role9, t8, TRUE, t10, role2>
    /*  20 */  <role9, t2, TRUE, t1, role2>
    /*  21 */  <role9, t6, TRUE, t2, role2>
    /*  22 */  <role9, t9, TRUE, t8, role2>
    /*  23 */  <role9, t3, TRUE, t3, role2>
    /*  24 */  <role3, t1, TRUE, t7, role14>
    /*  25 */  <role3, t9, TRUE, t6, role14>
    /*  26 */  <role3, t2, TRUE, t8, role14>
    /*  27 */  <role3, t4, TRUE, t1, role14>
    /*  28 */  <role3, t3, TRUE, t9, role14>
    /*  29 */  <role3, t5, TRUE, t2, role14>
    /*  30 */  <role3, t6, TRUE, t3, role14>
    /*  31 */  <role3, t7, TRUE, t10, role14>
    /*  32 */  <TRUE, t5, TRUE, t6, role4>
    /*  33 */  <role5, t4, TRUE, t7, role4>
    /*  34 */  <role5, t1, TRUE, t2, role4>
    /*  35 */  <role5, t2, TRUE, t3, role4>
    /*  36 */  <role5, t9, TRUE, t8, role4>
    /*  37 */  <role5, t10, TRUE, t10, role4>
    /*  38 */  <TRUE, t8, TRUE, t3, role6>
    /*  39 */  <role5, t1, TRUE, t4, role8>
    /*  40 */  <role5, t5, TRUE, t7, role8>
    /*  41 */  <role5, t6, TRUE, t5, role8>
    /*  42 */  <role5, t2, TRUE, t9, role8>
    /*  43 */  <TRUE, t3, TRUE, t6, role8>
    /*  44 */  <TRUE, t10, TRUE, t8, role8>
    /*  45 */  <TRUE, t7, TRUE, t10, role8>
    /*  46 */  <TRUE, t4, TRUE, t10, role2>
    /*  47 */  <role9, t5, TRUE, t4, role2>
    /*  48 */  <role9, t8, TRUE, t5, role2>
    /*  49 */  <TRUE, t6, TRUE, t2, role2>
    /*  50 */  <role9, t8, TRUE, t8, role2>
    /*  51 */  <role9, t8, TRUE, t3, role2>
    /*  52 */  <role9, t6, TRUE, t8, role2>
    /*  53 */  <role9, t7, TRUE, t3, role2>
    /*  54 */  <role9, t2, TRUE, t1, role2>
    /*  55 */  <role9, t3, TRUE, t8, role2>
    /*  56 */  <role9, t7, TRUE, t7, role2>
    /*  57 */  <role3, t1, role3, t5, role13>
    /*  58 */  <role6, t5, role3, t1, role7>
    /*  59 */  <role6, t1, role3, t5, role7>
    /*  60 */  <role6, t3, role3, t3, role7>
    /*  61 */  <role6, t4, role3, t6, role7>
    /*  62 */  <role6, t10, role3, t2, role7>
    /*  63 */  <TRUE, t8, role3, t4, role7>
    /*  64 */  <role6, t7, role3, t7, role7>
    /*  65 */  <role6, t9, role3, t10, role7>
    /*  66 */  <role6, t6, role3, t8, role7>
    /*  67 */  <role6, t2, role3, t9, role7>
    /*  68 */  <role6, t8, role8, t2, role7>
    /*  69 */  <role6, t6, role8, t7, role7>
    /*  70 */  <role6, t7, role8, t6, role7>
    /*  71 */  <role6, t9, role8, t3, role7>
    /*  72 */  <role6, t1, role8, t1, role7>
    /*  73 */  <role6, t10, role8, t10, role7>
    /*  74 */  <role6, t2, role8, t9, role7>
    /*  75 */  <role6, t3, role8, t4, role7>
    /*  76 */  <role6, t4, role8, t5, role7>
    /*  77 */  <role5, t2, NOT ~ role3, t1, role12>
    /*  78 */  <role5, t3, NOT ~ role3, t3, role12>
    /*  79 */  <role5, t10, NOT ~ role3, t8, role12>
    /*  80 */  <TRUE, t4, NOT ~ role3, t2, role12>
    /*  81 */  <role5, t1, NOT ~ role3, t5, role12>
    /*  82 */  <TRUE, t8, NOT ~ role3, t10, role12>
    /*  83 */  <role5, t5, NOT ~ role3, t6, role12>
    /*  84 */  <TRUE, t9, NOT ~ role3, t4, role12>
    /*  85 */  <TRUE, t6, NOT ~ role3, t7, role12>
    /*  86 */  <role12, t6, NOT ~ role11, t7, role9>
    /*  87 */  <role14, t4, role9, t7, role10>
    /*  88 */  <role14, t2, role9, t7, role10>
    /*  89 */  <role14, t1, role9, t7, role10>
    /*  90 */  <role14, t7, role9, t7, role10>
    /*  91 */  <TRUE, t8, NOT ~ role12, t3, role3>
    /*  92 */  <TRUE, t3, NOT ~ role12, t7, role3>
    /*  93 */  <role5, t5, NOT ~ role12, t9, role3>
    /*  94 */  <role5, t9, NOT ~ role12, t8, role3>
    /*  95 */  <TRUE, t6, NOT ~ role12, t10, role3>
    /*  96 */  <role5, t7, NOT ~ role12, t1, role3>
    /*  97 */  <role5, t4, NOT ~ role12, t5, role3>
    /*  98 */  <role5, t10, NOT ~ role12, t2, role3>
    /*  99 */  <role5, t1, NOT ~ role12, t4, role3>
    /* 100 */  <role5, t2, NOT ~ role12, t6, role3>
    /* 101 */  <role9, t4, role3 & NOT ~ role9, t2, role11>
    /* 102 */  <role9, t5, role3 & NOT ~ role9, t7, role11>
    /* 103 */  <role9, t3, role3 & NOT ~ role9, t3, role11>
    /* 104 */  <role9, t8, role3 & NOT ~ role9, t9, role11>
    /* 105 */  <TRUE, t8, NOT ~ role11, t10, role9>
    /* 106 */  <role12, t7, NOT ~ role11, t9, role9>
    /* 107 */  <TRUE, t3, NOT ~ role11, t6, role9>
    /* 108 */  <role12, t2, NOT ~ role11, t7, role9>
    /* 109 */  <role12, t4, NOT ~ role11, t4, role9>
    /* 110 */  <TRUE, t9, NOT ~ role11, t3, role9>
    /* 111 */  <role12, t5, NOT ~ role11, t8, role9>
    /* 112 */  <role12, t10, NOT ~ role11, t2, role9>
    /* 113 */  <role12, t6, NOT ~ role11, t5, role9>
    /* 114 */  <role14, t4, role9, t4, role10>
    /* 115 */  <role14, t2, role9, t5, role10>
    /* 116 */  <role14, t1, role9, t6, role10>
    /* 117 */  <role14, t7, role9, t3, role10>
}

/* 
 * Number of Rules       : 69
 * Largest Precondition  : 1
 * Largest Role Schedule : 1
 * Startable Rules       : 67
 * Truly Startable Rules : 12
 */
CanRevoke {
    /*  1 */  <TRUE, t6, TRUE, t8, role14>
    /*  2 */  <role3, t3, TRUE, t1, role14>
    /*  3 */  <role3, t4, TRUE, t9, role14>
    /*  4 */  <TRUE, t7, TRUE, t4, role14>
    /*  5 */  <role3, t8, TRUE, t10, role14>
    /*  6 */  <role3, t6, TRUE, t6, role13>
    /*  7 */  <role6, t1, TRUE, t10, role7>
    /*  8 */  <role6, t9, TRUE, t4, role7>
    /*  9 */  <role6, t6, TRUE, t3, role7>
    /* 10 */  <role6, t4, TRUE, t9, role7>
    /* 11 */  <role6, t10, TRUE, t7, role7>
    /* 12 */  <role6, t1, TRUE, t5, role7>
    /* 13 */  <TRUE, t7, TRUE, t6, role7>
    /* 14 */  <role6, t5, TRUE, t2, role7>
    /* 15 */  <role6, t8, TRUE, t8, role7>
    /* 16 */  <role6, t2, TRUE, t1, role7>
    /* 17 */  <role5, t10, TRUE, t8, role4>
    /* 18 */  <TRUE, t8, TRUE, t6, role4>
    /* 19 */  <role5, t1, TRUE, t10, role4>
    /* 20 */  <TRUE, t7, TRUE, t3, role4>
    /* 21 */  <TRUE, t9, TRUE, t7, role4>
    /* 22 */  <role5, t2, TRUE, t1, role4>
    /* 23 */  <role5, t8, TRUE, t8, role6>
    /* 24 */  <role5, t6, TRUE, t7, role6>
    /* 25 */  <role5, t1, TRUE, t7, role12>
    /* 26 */  <role5, t4, TRUE, t6, role12>
    /* 27 */  <role5, t2, TRUE, t8, role12>
    /* 28 */  <role5, t3, TRUE, t10, role12>
    /* 29 */  <role5, t5, TRUE, t9, role12>
    /* 30 */  <role5, t8, TRUE, t3, role12>
    /* 31 */  <role5, t6, TRUE, t1, role12>
    /* 32 */  <role5, t9, TRUE, t2, role12>
    /* 33 */  <role5, t7, TRUE, t4, role12>
    /* 34 */  <role5, t10, TRUE, t5, role12>
    /* 35 */  <role5, t4, TRUE, t6, role8>
    /* 36 */  <role5, t7, TRUE, t7, role8>
    /* 37 */  <role5, t10, TRUE, t5, role8>
    /* 38 */  <TRUE, t8, TRUE, t1, role8>
    /* 39 */  <role5, t9, TRUE, t8, role8>
    /* 40 */  <role5, t6, TRUE, t9, role8>
    /* 41 */  <role5, t1, TRUE, t10, role8>
    /* 42 */  <role5, t2, TRUE, t1, role3>
    /* 43 */  <TRUE, t8, TRUE, t3, role3>
    /* 44 */  <role5, t9, TRUE, t9, role3>
    /* 45 */  <role5, t5, TRUE, t10, role3>
    /* 46 */  <role5, t3, TRUE, t2, role3>
    /* 47 */  <role9, t3, TRUE, t4, role2>
    /* 48 */  <TRUE, t7, TRUE, t3, role2>
    /* 49 */  <role9, t8, TRUE, t10, role2>
    /* 50 */  <role9, t9, TRUE, t2, role2>
    /* 51 */  <TRUE, t10, TRUE, t8, role2>
    /* 52 */  <role9, t1, TRUE, t8, role11>
    /* 53 */  <role9, t2, TRUE, t6, role11>
    /* 54 */  <TRUE, t3, TRUE, t3, role11>
    /* 55 */  <role9, t9, TRUE, t7, role11>
    /* 56 */  <role9, t4, TRUE, t10, role11>
    /* 57 */  <TRUE, t8, role1, t3, role3>
    /* 58 */  <role3, t4, TRUE, t2, role9>
    /* 59 */  <role3, t6, TRUE, t6, role9>
    /* 60 */  <role3, t7, TRUE, t7, role9>
    /* 61 */  <role3, t3, TRUE, t3, role9>
    /* 62 */  <role3, t8, TRUE, t4, role9>
    /* 63 */  <role3, t9, TRUE, t9, role9>
    /* 64 */  <TRUE, t5, TRUE, t5, role9>
    /* 65 */  <role3, t2, TRUE, t8, role9>
    /* 66 */  <role3, t10, TRUE, t10, role9>
    /* 67 */  <role3, t1, TRUE, t1, role9>
    /* 68 */  <TRUE, t8, role3, t3, role3>
    /* 69 */  <role14, t9, TRUE, t10, role10>
}

/* 
 * Number of Rules       : 53
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 36
 * Truly Startable Rules : 7
 */
CanEnable {
    /*  1 */  <role3, t6, TRUE, t6, role13>
    /*  2 */  <role6, t9, TRUE, t4, role7>
    /*  3 */  <TRUE, t7, TRUE, t3, role4>
    /*  4 */  <role5, t6, TRUE, t1, role12>
    /*  5 */  <role5, t7, TRUE, t7, role8>
    /*  6 */  <role5, t10, TRUE, t5, role8>
    /*  7 */  <TRUE, t8, TRUE, t1, role8>
    /*  8 */  <role5, t9, TRUE, t8, role8>
    /*  9 */  <role5, t1, TRUE, t10, role8>
    /* 10 */  <role5, t2, TRUE, t1, role3>
    /* 11 */  <role9, t1, TRUE, t8, role11>
    /* 12 */  <role9, t9, TRUE, t7, role11>
    /* 13 */  <role9, t4, TRUE, t10, role11>
    /* 14 */  <role3, t3, TRUE, t3, role9>
    /* 15 */  <role3, t9, TRUE, t9, role9>
    /* 16 */  <role1, t4, role9 & role11, t4, role15>
    /* 17 */  <role1, t8, role9 & role11, t5, role15>
    /* 18 */  <role6, t4, role8, t1, role7>
    /* 19 */  <role6, t5, role8, t5, role7>
    /* 20 */  <TRUE, t6, NOT ~ role3, t10, role12>
    /* 21 */  <role5, t8, NOT ~ role3, t6, role12>
    /* 22 */  <role5, t1, TRUE, t4, role4>
    /* 23 */  <role6, t9, role8, t5, role7>
    /* 24 */  <role6, t9, role8, t7, role7>
    /* 25 */  <role6, t9, role8, t4, role7>
    /* 26 */  <role3, t3, TRUE, t4, role9>
    /* 27 */  <role3, t9, TRUE, t4, role9>
    /* 28 */  <role1, t4, role9 & role11, t6, role15>
    /* 29 */  <role1, t8, role9 & role11, t5, role15>
    /* 30 */  <role6, t4, role8, t1, role7>
    /* 31 */  <role3, t3, TRUE, t9, role14>
    /* 32 */  <role3, t7, TRUE, t10, role14>
    /* 33 */  <role5, t4, TRUE, t7, role4>
    /* 34 */  <role5, t10, TRUE, t10, role4>
    /* 35 */  <role5, t5, TRUE, t7, role8>
    /* 36 */  <role9, t5, TRUE, t4, role2>
    /* 37 */  <TRUE, t6, TRUE, t2, role2>
    /* 38 */  <role9, t2, TRUE, t1, role2>
    /* 39 */  <role6, t6, role8, t7, role7>
    /* 40 */  <role6, t9, role8, t3, role7>
    /* 41 */  <role6, t1, role8, t1, role7>
    /* 42 */  <role6, t4, role8, t5, role7>
    /* 43 */  <TRUE, t8, NOT ~ role3, t10, role12>
    /* 44 */  <role5, t5, NOT ~ role3, t6, role12>
    /* 45 */  <TRUE, t3, NOT ~ role12, t7, role3>
    /* 46 */  <role5, t5, NOT ~ role12, t9, role3>
    /* 47 */  <role5, t9, NOT ~ role12, t8, role3>
    /* 48 */  <TRUE, t6, NOT ~ role12, t10, role3>
    /* 49 */  <role9, t4, role3 & NOT ~ role9, t2, role11>
    /* 50 */  <role12, t2, NOT ~ role11, t7, role9>
    /* 51 */  <role12, t6, NOT ~ role11, t5, role9>
    /* 52 */  <role14, t4, role9, t4, role10>
    /* 53 */  <role14, t1, role9, t6, role10>
}

/* 
 * Number of Rules       : 28
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 21
 * Truly Startable Rules : 5
 */
CanDisable {
    /*  1 */  <role6, t1, TRUE, t5, role7>
    /*  2 */  <role6, t5, TRUE, t2, role7>
    /*  3 */  <role6, t8, TRUE, t8, role7>
    /*  4 */  <role5, t1, TRUE, t10, role4>
    /*  5 */  <TRUE, t9, TRUE, t7, role4>
    /*  6 */  <role5, t9, TRUE, t9, role3>
    /*  7 */  <role9, t2, TRUE, t6, role11>
    /*  8 */  <TRUE, t5, TRUE, t5, role9>
    /*  9 */  <role3, t2, TRUE, t8, role9>
    /* 10 */  <role5, t4, NOT ~ role3, t1, role12>
    /* 11 */  <role5, t1, NOT ~ role3, t8, role12>
    /* 12 */  <role6, t7, role8, t5, role7>
    /* 13 */  <role6, t7, role8, t7, role7>
    /* 14 */  <role6, t7, role8, t4, role7>
    /* 15 */  <TRUE, t5, TRUE, t4, role9>
    /* 16 */  <role3, t2, TRUE, t4, role9>
    /* 17 */  <role3, t5, TRUE, t1, role14>
    /* 18 */  <role3, t6, TRUE, t3, role14>
    /* 19 */  <TRUE, t5, TRUE, t6, role4>
    /* 20 */  <role9, t8, TRUE, t5, role2>
    /* 21 */  <role3, t1, role3, t5, role13>
    /* 22 */  <role6, t7, role8, t6, role7>
    /* 23 */  <role5, t2, NOT ~ role3, t1, role12>
    /* 24 */  <role5, t10, NOT ~ role3, t8, role12>
    /* 25 */  <TRUE, t9, NOT ~ role3, t4, role12>
    /* 26 */  <role5, t1, NOT ~ role12, t4, role3>
    /* 27 */  <role9, t3, role3 & NOT ~ role9, t3, role11>
    /* 28 */  <role14, t7, role9, t3, role10>
}