/* Generated On        : 2015/02/07 20:24:43.724
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 30
 * Number of Timeslots : 20
 * Number of Rules     : 560
 * 
 * Roles     : |Size=30| [role1, role2, role3, role4, role5, role6, role7, role8, role11, role12, role13, role14, role15, role16, role17, role18, role19, role20, role21, role22, role23, role24, role25, role27, role28, role29, role30, role32, role33, role34]
 * Timeslots : |Size=20| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17, t18, t19, t20]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t9, [role15]

Expected: UNKNOWN

/* 
 * Number of Rules       : 405
 * Largest Precondition  : 5
 * Largest Role Schedule : 1
 * Startable Rules       : 94
 * Truly Startable Rules : 17
 */
CanAssign {
    /*   1 */  <TRUE, t13, role14 & role32, t2, role34>
    /*   2 */  <role1, t5, role14 & role32, t18, role34>
    /*   3 */  <role1, t14, role14 & role32, t6, role34>
    /*   4 */  <role1, t15, role14 & role32, t7, role34>
    /*   5 */  <role1, t7, role14 & role32, t3, role34>
    /*   6 */  <role1, t17, role14 & role32, t19, role34>
    /*   7 */  <role1, t20, role14 & role32, t8, role34>
    /*   8 */  <role1, t11, role14 & role32, t17, role34>
    /*   9 */  <role1, t1, role14 & role32, t9, role34>
    /*  10 */  <role1, t6, role14 & role32, t10, role34>
    /*  11 */  <TRUE, t2, role14 & role32, t12, role34>
    /*  12 */  <role1, t4, role14 & role32, t16, role34>
    /*  13 */  <TRUE, t12, role14 & role32, t11, role34>
    /*  14 */  <role1, t16, role14 & role32, t14, role34>
    /*  15 */  <role22, t2, TRUE, t20, role23>
    /*  16 */  <role22, t20, TRUE, t15, role23>
    /*  17 */  <role22, t3, TRUE, t11, role23>
    /*  18 */  <TRUE, t6, TRUE, t3, role23>
    /*  19 */  <role22, t6, TRUE, t19, role28>
    /*  20 */  <role22, t2, TRUE, t13, role7>
    /*  21 */  <TRUE, t19, TRUE, t12, role7>
    /*  22 */  <role22, t10, TRUE, t19, role7>
    /*  23 */  <role22, t1, TRUE, t11, role7>
    /*  24 */  <TRUE, t5, TRUE, t14, role21>
    /*  25 */  <TRUE, t8, TRUE, t13, role21>
    /*  26 */  <role22, t6, TRUE, t5, role21>
    /*  27 */  <TRUE, t7, TRUE, t15, role21>
    /*  28 */  <role22, t6, TRUE, t11, role3>
    /*  29 */  <role22, t1, TRUE, t19, role3>
    /*  30 */  <role22, t3, TRUE, t18, role3>
    /*  31 */  <role22, t2, TRUE, t15, role3>
    /*  32 */  <role22, t14, TRUE, t10, role3>
    /*  33 */  <role22, t4, TRUE, t6, role3>
    /*  34 */  <role22, t18, TRUE, t7, role3>
    /*  35 */  <role22, t9, TRUE, t9, role3>
    /*  36 */  <TRUE, t11, TRUE, t13, role3>
    /*  37 */  <role22, t19, TRUE, t16, role3>
    /*  38 */  <TRUE, t13, TRUE, t16, role4>
    /*  39 */  <role22, t1, TRUE, t18, role4>
    /*  40 */  <role22, t7, TRUE, t7, role4>
    /*  41 */  <TRUE, t16, TRUE, t9, role4>
    /*  42 */  <TRUE, t14, TRUE, t11, role4>
    /*  43 */  <TRUE, t1, TRUE, t17, role23>
    /*  44 */  <role22, t15, TRUE, t3, role23>
    /*  45 */  <role22, t16, TRUE, t16, role23>
    /*  46 */  <role22, t7, TRUE, t2, role23>
    /*  47 */  <role22, t14, TRUE, t4, role23>
    /*  48 */  <TRUE, t17, TRUE, t5, role23>
    /*  49 */  <role22, t13, TRUE, t9, role23>
    /*  50 */  <TRUE, t6, TRUE, t8, role23>
    /*  51 */  <TRUE, t18, TRUE, t6, role23>
    /*  52 */  <TRUE, t1, TRUE, t12, role28>
    /*  53 */  <role22, t6, TRUE, t13, role28>
    /*  54 */  <role22, t4, TRUE, t11, role28>
    /*  55 */  <role22, t11, TRUE, t20, role7>
    /*  56 */  <role22, t15, TRUE, t15, role7>
    /*  57 */  <role22, t17, TRUE, t3, role7>
    /*  58 */  <role22, t10, TRUE, t12, role7>
    /*  59 */  <role22, t5, TRUE, t1, role7>
    /*  60 */  <role22, t12, TRUE, t2, role7>
    /*  61 */  <role22, t13, TRUE, t4, role7>
    /*  62 */  <role22, t6, TRUE, t5, role7>
    /*  63 */  <role22, t4, TRUE, t12, role21>
    /*  64 */  <role22, t8, TRUE, t6, role21>
    /*  65 */  <role22, t3, TRUE, t13, role21>
    /*  66 */  <role22, t18, TRUE, t4, role21>
    /*  67 */  <role22, t5, TRUE, t10, role21>
    /*  68 */  <role22, t1, TRUE, t5, role21>
    /*  69 */  <role22, t14, TRUE, t19, role21>
    /*  70 */  <role22, t6, TRUE, t18, role21>
    /*  71 */  <role22, t7, TRUE, t7, role21>
    /*  72 */  <role22, t15, TRUE, t14, role21>
    /*  73 */  <role22, t9, TRUE, t11, role21>
    /*  74 */  <role22, t10, TRUE, t16, role21>
    /*  75 */  <role22, t11, TRUE, t15, role21>
    /*  76 */  <role22, t12, TRUE, t17, role21>
    /*  77 */  <role22, t15, TRUE, t15, role3>
    /*  78 */  <role22, t16, TRUE, t12, role3>
    /*  79 */  <role22, t5, TRUE, t1, role3>
    /*  80 */  <role22, t9, TRUE, t13, role3>
    /*  81 */  <role22, t13, TRUE, t14, role3>
    /*  82 */  <role22, t20, TRUE, t5, role3>
    /*  83 */  <TRUE, t6, TRUE, t9, role3>
    /*  84 */  <role22, t17, TRUE, t2, role3>
    /*  85 */  <role22, t7, TRUE, t8, role3>
    /*  86 */  <role22, t11, TRUE, t11, role3>
    /*  87 */  <role22, t3, TRUE, t7, role3>
    /*  88 */  <role22, t12, TRUE, t20, role4>
    /*  89 */  <role2, t15, NOT ~ role14, t11, role32>
    /*  90 */  <role2, t19, NOT ~ role14, t7, role32>
    /*  91 */  <role2, t6, NOT ~ role14, t16, role32>
    /*  92 */  <role2, t3, NOT ~ role14, t4, role32>
    /*  93 */  <role2, t17, NOT ~ role14, t12, role32>
    /*  94 */  <role2, t4, NOT ~ role14, t15, role32>
    /*  95 */  <TRUE, t20, NOT ~ role14, t2, role32>
    /*  96 */  <TRUE, t13, role17, t18, role16>
    /*  97 */  <TRUE, t1, role17, t20, role16>
    /*  98 */  <role8, t7, role14, t12, role18>
    /*  99 */  <role8, t14, role14, t2, role18>
    /* 100 */  <role8, t20, role14, t1, role18>
    /* 101 */  <role8, t4, role14, t19, role18>
    /* 102 */  <TRUE, t8, role14, t3, role18>
    /* 103 */  <role8, t10, role14, t10, role18>
    /* 104 */  <role8, t2, role14, t18, role18>
    /* 105 */  <role8, t6, role14, t9, role18>
    /* 106 */  <TRUE, t3, role14, t5, role18>
    /* 107 */  <role8, t15, role32, t13, role18>
    /* 108 */  <role8, t17, role32, t20, role18>
    /* 109 */  <TRUE, t8, role32, t6, role18>
    /* 110 */  <role8, t6, role32, t8, role18>
    /* 111 */  <TRUE, t16, role32, t4, role18>
    /* 112 */  <TRUE, t4, role32, t11, role18>
    /* 113 */  <role8, t19, role32, t17, role18>
    /* 114 */  <TRUE, t18, role32, t1, role18>
    /* 115 */  <role8, t20, role32, t12, role18>
    /* 116 */  <role12, t9, role29, t2, role27>
    /* 117 */  <TRUE, t6, role29, t1, role27>
    /* 118 */  <role12, t1, role29, t17, role27>
    /* 119 */  <role12, t10, role29, t14, role27>
    /* 120 */  <role12, t11, role29, t15, role27>
    /* 121 */  <role12, t17, role29, t20, role27>
    /* 122 */  <role12, t6, role14, t3, role27>
    /* 123 */  <TRUE, t14, role14, t1, role27>
    /* 124 */  <role12, t16, role14, t13, role27>
    /* 125 */  <role12, t3, role14, t14, role27>
    /* 126 */  <role12, t17, role14, t16, role27>
    /* 127 */  <TRUE, t5, role14, t17, role27>
    /* 128 */  <TRUE, t12, role32, t14, role27>
    /* 129 */  <role12, t13, role32, t3, role27>
    /* 130 */  <role12, t8, role32, t12, role27>
    /* 131 */  <role12, t19, role32, t10, role27>
    /* 132 */  <role12, t10, role32, t8, role27>
    /* 133 */  <role12, t14, role32, t19, role27>
    /* 134 */  <TRUE, t11, role32, t20, role27>
    /* 135 */  <role12, t16, role32, t13, role27>
    /* 136 */  <role15, t4, NOT ~ role32, t18, role14>
    /* 137 */  <role15, t14, NOT ~ role32, t1, role14>
    /* 138 */  <role15, t11, NOT ~ role32, t6, role14>
    /* 139 */  <role15, t6, NOT ~ role32, t4, role14>
    /* 140 */  <role15, t7, NOT ~ role32, t13, role14>
    /* 141 */  <TRUE, t16, NOT ~ role32, t16, role14>
    /* 142 */  <role15, t8, NOT ~ role32, t5, role14>
    /* 143 */  <role15, t9, NOT ~ role32, t12, role14>
    /* 144 */  <role15, t10, NOT ~ role32, t19, role14>
    /* 145 */  <role15, t3, NOT ~ role32, t3, role14>
    /* 146 */  <role15, t12, NOT ~ role32, t20, role14>
    /* 147 */  <role15, t18, NOT ~ role32, t7, role14>
    /* 148 */  <role15, t1, NOT ~ role32, t14, role14>
    /* 149 */  <TRUE, t17, role14, t19, role20>
    /* 150 */  <role19, t9, role14, t4, role20>
    /* 151 */  <TRUE, t10, role14, t12, role20>
    /* 152 */  <role19, t4, role14, t11, role20>
    /* 153 */  <role19, t11, role14, t16, role20>
    /* 154 */  <role19, t18, role14, t17, role20>
    /* 155 */  <role19, t5, role14, t18, role20>
    /* 156 */  <role19, t7, role14, t5, role20>
    /* 157 */  <role19, t6, role14, t6, role20>
    /* 158 */  <role19, t3, role14, t20, role20>
    /* 159 */  <role19, t14, role14, t9, role20>
    /* 160 */  <role19, t15, role14, t1, role20>
    /* 161 */  <role19, t1, role14, t13, role20>
    /* 162 */  <role19, t12, role14, t15, role20>
    /* 163 */  <TRUE, t8, role14, t2, role20>
    /* 164 */  <role19, t5, role32, t20, role20>
    /* 165 */  <TRUE, t6, role14, t16, role33>
    /* 166 */  <role12, t20, role14, t4, role33>
    /* 167 */  <role12, t3, role14, t10, role33>
    /* 168 */  <role12, t7, role14, t18, role33>
    /* 169 */  <role12, t2, role14, t19, role33>
    /* 170 */  <role12, t1, role14, t11, role33>
    /* 171 */  <role12, t11, role32, t6, role33>
    /* 172 */  <role12, t10, role32, t10, role33>
    /* 173 */  <role12, t5, role32, t1, role33>
    /* 174 */  <role6, t19, role2, t18, role5>
    /* 175 */  <role6, t2, role2, t6, role5>
    /* 176 */  <role6, t17, role22, t19, role5>
    /* 177 */  <TRUE, t8, role22, t17, role5>
    /* 178 */  <role6, t7, role22, t12, role5>
    /* 179 */  <role6, t9, role22, t7, role5>
    /* 180 */  <role6, t13, role22, t9, role5>
    /* 181 */  <role6, t15, role22, t14, role5>
    /* 182 */  <role6, t10, role22, t18, role5>
    /* 183 */  <role6, t11, role22, t8, role5>
    /* 184 */  <TRUE, t12, role22, t6, role5>
    /* 185 */  <TRUE, t14, role22, t13, role5>
    /* 186 */  <role6, t16, role22, t5, role5>
    /* 187 */  <role6, t18, role22, t20, role5>
    /* 188 */  <role6, t20, role22, t15, role5>
    /* 189 */  <role6, t6, role22, t1, role5>
    /* 190 */  <role6, t7, role24, t11, role5>
    /* 191 */  <role6, t15, role24, t4, role5>
    /* 192 */  <role6, t2, role24, t12, role5>
    /* 193 */  <role6, t16, role24, t13, role5>
    /* 194 */  <role6, t8, role24, t1, role5>
    /* 195 */  <role6, t17, role24, t15, role5>
    /* 196 */  <role6, t18, role24, t19, role5>
    /* 197 */  <TRUE, t4, role24, t5, role5>
    /* 198 */  <role6, t3, role24, t20, role5>
    /* 199 */  <role6, t14, role24, t6, role5>
    /* 200 */  <role6, t8, role7, t15, role5>
    /* 201 */  <role6, t1, role7, t13, role5>
    /* 202 */  <role6, t5, role7, t3, role5>
    /* 203 */  <role6, t5, role28, t6, role5>
    /* 204 */  <role6, t7, role28, t3, role5>
    /* 205 */  <TRUE, t1, role28, t5, role5>
    /* 206 */  <role6, t12, role28, t19, role5>
    /* 207 */  <role6, t11, role28, t1, role5>
    /* 208 */  <role6, t8, role28, t20, role5>
    /* 209 */  <role6, t16, role28, t9, role5>
    /* 210 */  <TRUE, t4, role28, t2, role5>
    /* 211 */  <role6, t9, role28, t4, role5>
    /* 212 */  <role6, t14, role28, t18, role5>
    /* 213 */  <role6, t3, role28, t7, role5>
    /* 214 */  <role6, t18, role28, t8, role5>
    /* 215 */  <role6, t10, role28, t17, role5>
    /* 216 */  <TRUE, t13, role28, t10, role5>
    /* 217 */  <role7, t15, role22 & NOT ~ role15, t6, role2>
    /* 218 */  <role7, t4, role22 & NOT ~ role15, t20, role2>
    /* 219 */  <role7, t12, role24 & NOT ~ role15, t5, role2>
    /* 220 */  <role7, t17, role24 & NOT ~ role15, t2, role2>
    /* 221 */  <role7, t3, role24 & NOT ~ role15, t14, role2>
    /* 222 */  <role7, t9, role24 & NOT ~ role15, t3, role2>
    /* 223 */  <role7, t4, role24 & NOT ~ role15, t6, role2>
    /* 224 */  <role7, t16, role24 & NOT ~ role15, t10, role2>
    /* 225 */  <role7, t5, role24 & NOT ~ role15, t4, role2>
    /* 226 */  <role7, t19, role24 & NOT ~ role15, t1, role2>
    /* 227 */  <TRUE, t6, role24 & NOT ~ role15, t7, role2>
    /* 228 */  <TRUE, t20, role24 & NOT ~ role15, t8, role2>
    /* 229 */  <role7, t15, role24 & NOT ~ role15, t15, role2>
    /* 230 */  <TRUE, t13, role24 & NOT ~ role15, t18, role2>
    /* 231 */  <role7, t14, role24 & NOT ~ role15, t16, role2>
    /* 232 */  <role7, t8, role24 & NOT ~ role15, t12, role2>
    /* 233 */  <role7, t4, role7 & NOT ~ role15, t3, role2>
    /* 234 */  <TRUE, t7, role7 & NOT ~ role15, t17, role2>
    /* 235 */  <role7, t1, role7 & NOT ~ role15, t14, role2>
    /* 236 */  <role7, t15, role7 & NOT ~ role15, t15, role2>
    /* 237 */  <role7, t5, role7 & NOT ~ role15, t16, role2>
    /* 238 */  <role7, t3, role7 & NOT ~ role15, t12, role2>
    /* 239 */  <role7, t9, role7 & NOT ~ role15, t19, role2>
    /* 240 */  <role7, t2, role7 & NOT ~ role15, t20, role2>
    /* 241 */  <role7, t6, role7 & NOT ~ role15, t18, role2>
    /* 242 */  <role7, t8, role7 & NOT ~ role15, t2, role2>
    /* 243 */  <role7, t10, role7 & NOT ~ role15, t4, role2>
    /* 244 */  <role7, t20, role7 & NOT ~ role15, t6, role2>
    /* 245 */  <role7, t11, role7 & NOT ~ role15, t7, role2>
    /* 246 */  <role7, t12, role7 & NOT ~ role15, t5, role2>
    /* 247 */  <role7, t20, role28 & NOT ~ role15, t20, role2>
    /* 248 */  <TRUE, t11, role28 & NOT ~ role15, t16, role2>
    /* 249 */  <role7, t1, role28 & NOT ~ role15, t6, role2>
    /* 250 */  <role7, t7, role28 & NOT ~ role15, t5, role2>
    /* 251 */  <role7, t8, role28 & NOT ~ role15, t13, role2>
    /* 252 */  <role7, t9, role28 & NOT ~ role15, t7, role2>
    /* 253 */  <role7, t16, role28 & NOT ~ role15, t18, role2>
    /* 254 */  <role7, t2, role28 & NOT ~ role15, t17, role2>
    /* 255 */  <TRUE, t3, role28 & NOT ~ role15, t1, role2>
    /* 256 */  <role7, t12, role28 & NOT ~ role15, t19, role2>
    /* 257 */  <role7, t6, role28 & NOT ~ role15, t3, role2>
    /* 258 */  <TRUE, t19, role28 & NOT ~ role15, t15, role2>
    /* 259 */  <role7, t4, role28 & NOT ~ role15, t2, role2>
    /* 260 */  <TRUE, t18, role28 & NOT ~ role15, t4, role2>
    /* 261 */  <role7, t13, role28 & NOT ~ role15, t8, role2>
    /* 262 */  <role8, t11, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t15, role15>
    /* 263 */  <role8, t6, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 264 */  <role8, t8, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t13, role15>
    /* 265 */  <role8, t12, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 266 */  <role8, t13, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 267 */  <role8, t7, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 268 */  <role8, t10, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t14, role15>
    /* 269 */  <role8, t15, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 270 */  <role8, t1, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 271 */  <role8, t9, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t16, role15>
    /* 272 */  <role8, t16, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t17, role15>
    /* 273 */  <role8, t17, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 274 */  <TRUE, t14, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 275 */  <role8, t18, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t18, role15>
    /* 276 */  <role8, t19, role3 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t11, role15>
    /* 277 */  <role8, t5, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 278 */  <role8, t6, role4 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 279 */  <role8, t16, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 280 */  <role8, t17, role21 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t14, role15>
    /* 281 */  <role8, t18, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 282 */  <TRUE, t8, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t15, role15>
    /* 283 */  <role8, t15, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 284 */  <role8, t2, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 285 */  <role8, t1, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t16, role15>
    /* 286 */  <TRUE, t19, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t2, role15>
    /* 287 */  <role8, t3, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 288 */  <TRUE, t20, role6 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 289 */  <role8, t18, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t6, role15>
    /* 290 */  <role8, t4, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t11, role15>
    /* 291 */  <role8, t19, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 292 */  <role8, t20, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t16, role15>
    /* 293 */  <TRUE, t15, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 294 */  <TRUE, t2, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 295 */  <role8, t3, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 296 */  <role8, t7, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t19, role15>
    /* 297 */  <role8, t16, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t15, role15>
    /* 298 */  <role8, t5, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t10, role15>
    /* 299 */  <role8, t1, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 300 */  <role8, t10, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t12, role15>
    /* 301 */  <role8, t6, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t13, role15>
    /* 302 */  <role8, t8, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 303 */  <role8, t9, role23 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t17, role15>
    /* 304 */  <TRUE, t9, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 305 */  <TRUE, t14, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t7, role15>
    /* 306 */  <role8, t1, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t8, role15>
    /* 307 */  <TRUE, t13, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t18, role15>
    /* 308 */  <role8, t3, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t14, role15>
    /* 309 */  <role8, t17, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t1, role15>
    /* 310 */  <TRUE, t11, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t4, role15>
    /* 311 */  <role8, t12, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t12, role15>
    /* 312 */  <TRUE, t5, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t9, role15>
    /* 313 */  <role8, t2, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t20, role15>
    /* 314 */  <role8, t16, role8 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t5, role15>
    /* 315 */  <role8, t19, role12 & NOT ~ role2 & NOT ~ role7 & NOT ~ role22 & NOT ~ role24, t3, role15>
    /* 316 */  <role8, t16, role3, t20, role16>
    /* 317 */  <role8, t7, role3, t13, role16>
    /* 318 */  <role8, t6, role3, t6, role16>
    /* 319 */  <TRUE, t15, role3, t3, role16>
    /* 320 */  <TRUE, t17, role3, t15, role16>
    /* 321 */  <role8, t1, role3, t5, role16>
    /* 322 */  <TRUE, t2, role3, t1, role16>
    /* 323 */  <role8, t18, role3, t2, role16>
    /* 324 */  <role8, t19, role3, t14, role16>
    /* 325 */  <role8, t20, role3, t11, role16>
    /* 326 */  <role8, t11, role3, t18, role16>
    /* 327 */  <role8, t13, role3, t8, role16>
    /* 328 */  <role8, t3, role3, t9, role16>
    /* 329 */  <role8, t18, role4, t16, role16>
    /* 330 */  <role8, t12, role4, t14, role16>
    /* 331 */  <TRUE, t13, role4, t8, role16>
    /* 332 */  <role8, t16, role4, t15, role16>
    /* 333 */  <role8, t8, role4, t17, role16>
    /* 334 */  <role8, t12, role22, t4, role16>
    /* 335 */  <TRUE, t13, role22, t15, role16>
    /* 336 */  <role8, t18, role22, t5, role16>
    /* 337 */  <role8, t5, role22, t6, role16>
    /* 338 */  <role8, t7, role22, t19, role16>
    /* 339 */  <role8, t19, role22, t2, role16>
    /* 340 */  <TRUE, t4, role6, t20, role16>
    /* 341 */  <TRUE, t5, role6, t13, role16>
    /* 342 */  <role8, t13, role6, t14, role16>
    /* 343 */  <TRUE, t6, role6, t18, role16>
    /* 344 */  <role8, t20, role6, t19, role16>
    /* 345 */  <role8, t7, role6, t1, role16>
    /* 346 */  <role8, t8, role6, t16, role16>
    /* 347 */  <role8, t9, role6, t4, role16>
    /* 348 */  <TRUE, t10, role6, t17, role16>
    /* 349 */  <role8, t2, role6, t2, role16>
    /* 350 */  <role8, t11, role6, t3, role16>
    /* 351 */  <role8, t12, role6, t8, role16>
    /* 352 */  <role8, t5, role24, t12, role16>
    /* 353 */  <role8, t13, role24, t17, role16>
    /* 354 */  <role8, t16, role24, t20, role16>
    /* 355 */  <role8, t18, role24, t18, role16>
    /* 356 */  <role8, t13, role23, t11, role16>
    /* 357 */  <TRUE, t19, role23, t7, role16>
    /* 358 */  <role8, t16, role23, t19, role16>
    /* 359 */  <role8, t20, role23, t15, role16>
    /* 360 */  <role8, t14, role23, t14, role16>
    /* 361 */  <TRUE, t15, role23, t20, role16>
    /* 362 */  <role8, t12, role23, t4, role16>
    /* 363 */  <TRUE, t11, role23, t9, role16>
    /* 364 */  <TRUE, t8, role3, t19, role19>
    /* 365 */  <role8, t10, role8, t13, role11>
    /* 366 */  <role8, t2, role8, t15, role11>
    /* 367 */  <role8, t13, role8, t14, role11>
    /* 368 */  <role8, t12, role8, t5, role11>
    /* 369 */  <role8, t14, role8, t6, role11>
    /* 370 */  <role8, t5, role8, t4, role11>
    /* 371 */  <role8, t16, role8, t7, role11>
    /* 372 */  <role8, t4, role8, t8, role11>
    /* 373 */  <role8, t6, role8, t9, role11>
    /* 374 */  <role8, t17, role8, t16, role11>
    /* 375 */  <role8, t6, role12, t10, role11>
    /* 376 */  <TRUE, t20, role12, t8, role11>
    /* 377 */  <role8, t18, role12, t16, role11>
    /* 378 */  <TRUE, t8, role12, t15, role11>
    /* 379 */  <TRUE, t3, role12, t17, role11>
    /* 380 */  <role8, t5, role12, t9, role11>
    /* 381 */  <role8, t16, role12, t7, role11>
    /* 382 */  <role8, t20, role3, t6, role25>
    /* 383 */  <role8, t14, role3, t8, role25>
    /* 384 */  <role8, t18, role3, t9, role25>
    /* 385 */  <role8, t2, role3, t13, role25>
    /* 386 */  <role8, t5, role3, t17, role25>
    /* 387 */  <role8, t9, role3, t7, role25>
    /* 388 */  <TRUE, t7, role3, t3, role25>
    /* 389 */  <TRUE, t12, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t16, role24>
    /* 390 */  <role22, t14, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t4, role24>
    /* 391 */  <role22, t4, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t19, role24>
    /* 392 */  <role22, t1, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t15, role24>
    /* 393 */  <role22, t19, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t17, role24>
    /* 394 */  <role24, t15, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t17, role6>
    /* 395 */  <role24, t1, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t6, role6>
    /* 396 */  <role24, t14, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t1, role6>
    /* 397 */  <role24, t11, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t14, role6>
    /* 398 */  <role24, t4, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t13, role6>
    /* 399 */  <role6, t7, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t2, role8>
    /* 400 */  <role6, t14, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t15, role8>
    /* 401 */  <role6, t18, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t16, role8>
    /* 402 */  <role6, t19, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t3, role8>
    /* 403 */  <TRUE, t2, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t20, role8>
    /* 404 */  <role6, t3, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t1, role8>
    /* 405 */  <TRUE, t4, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t8, role8>
}

/* 
 * Number of Rules       : 60
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 60
 * Truly Startable Rules : 15
 */
CanRevoke {
    /*  1 */  <role6, t8, TRUE, t20, role32>
    /*  2 */  <role6, t16, TRUE, t17, role32>
    /*  3 */  <role6, t11, TRUE, t10, role32>
    /*  4 */  <TRUE, t12, TRUE, t15, role32>
    /*  5 */  <role6, t4, TRUE, t9, role32>
    /*  6 */  <TRUE, t17, TRUE, t11, role32>
    /*  7 */  <TRUE, t10, TRUE, t8, role32>
    /*  8 */  <TRUE, t1, TRUE, t4, role32>
    /*  9 */  <role8, t18, TRUE, t16, role30>
    /* 10 */  <role8, t8, TRUE, t2, role30>
    /* 11 */  <role8, t16, TRUE, t17, role30>
    /* 12 */  <role8, t10, TRUE, t5, role30>
    /* 13 */  <role8, t2, TRUE, t18, role30>
    /* 14 */  <role8, t2, TRUE, t10, role17>
    /* 15 */  <TRUE, t15, TRUE, t18, role17>
    /* 16 */  <TRUE, t19, TRUE, t19, role17>
    /* 17 */  <role8, t18, TRUE, t15, role17>
    /* 18 */  <role8, t16, TRUE, t1, role17>
    /* 19 */  <role8, t9, TRUE, t9, role17>
    /* 20 */  <TRUE, t12, TRUE, t19, role16>
    /* 21 */  <role8, t10, TRUE, t1, role16>
    /* 22 */  <TRUE, t13, TRUE, t14, role16>
    /* 23 */  <role19, t8, TRUE, t16, role20>
    /* 24 */  <role19, t14, TRUE, t4, role20>
    /* 25 */  <role19, t5, TRUE, t8, role20>
    /* 26 */  <role19, t16, TRUE, t18, role20>
    /* 27 */  <role19, t9, TRUE, t14, role20>
    /* 28 */  <role19, t10, TRUE, t9, role20>
    /* 29 */  <role19, t6, TRUE, t10, role20>
    /* 30 */  <role19, t7, TRUE, t13, role20>
    /* 31 */  <TRUE, t19, TRUE, t18, role33>
    /* 32 */  <role12, t20, TRUE, t11, role33>
    /* 33 */  <role12, t1, TRUE, t19, role33>
    /* 34 */  <role12, t12, TRUE, t10, role33>
    /* 35 */  <role12, t9, TRUE, t9, role33>
    /* 36 */  <role12, t2, TRUE, t15, role33>
    /* 37 */  <TRUE, t10, TRUE, t17, role33>
    /* 38 */  <role12, t3, TRUE, t5, role33>
    /* 39 */  <TRUE, t2, TRUE, t10, role5>
    /* 40 */  <role6, t15, TRUE, t20, role5>
    /* 41 */  <role6, t3, TRUE, t9, role5>
    /* 42 */  <TRUE, t9, TRUE, t18, role5>
    /* 43 */  <TRUE, t13, TRUE, t8, role2>
    /* 44 */  <role22, t17, TRUE, t5, role13>
    /* 45 */  <role22, t16, TRUE, t19, role13>
    /* 46 */  <role22, t2, TRUE, t7, role13>
    /* 47 */  <role22, t17, TRUE, t18, role24>
    /* 48 */  <role22, t12, TRUE, t8, role24>
    /* 49 */  <role22, t3, TRUE, t9, role24>
    /* 50 */  <role22, t9, TRUE, t10, role24>
    /* 51 */  <role22, t6, TRUE, t19, role24>
    /* 52 */  <role22, t18, TRUE, t13, role24>
    /* 53 */  <role24, t11, TRUE, t13, role6>
    /* 54 */  <role24, t13, TRUE, t2, role6>
    /* 55 */  <role24, t16, TRUE, t9, role6>
    /* 56 */  <role6, t8, TRUE, t8, role8>
    /* 57 */  <TRUE, t4, TRUE, t7, role8>
    /* 58 */  <role6, t7, TRUE, t10, role8>
    /* 59 */  <role6, t9, TRUE, t16, role8>
    /* 60 */  <TRUE, t10, TRUE, t18, role8>
}

/* 
 * Number of Rules       : 59
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 32
 * Truly Startable Rules : 5
 */
CanEnable {
    /*  1 */  <role8, t8, TRUE, t2, role30>
    /*  2 */  <role8, t9, TRUE, t9, role17>
    /*  3 */  <role8, t10, TRUE, t1, role16>
    /*  4 */  <role8, t14, TRUE, t13, role16>
    /*  5 */  <TRUE, t15, TRUE, t5, role16>
    /*  6 */  <role6, t6, TRUE, t6, role14>
    /*  7 */  <role6, t18, TRUE, t2, role14>
    /*  8 */  <role6, t7, TRUE, t13, role14>
    /*  9 */  <role19, t13, TRUE, t17, role20>
    /* 10 */  <role22, t6, TRUE, t19, role24>
    /* 11 */  <role24, t13, TRUE, t2, role6>
    /* 12 */  <role24, t16, TRUE, t9, role6>
    /* 13 */  <role1, t14, role14 & role32, t6, role34>
    /* 14 */  <role1, t7, role14 & role32, t3, role34>
    /* 15 */  <role1, t20, role14 & role32, t8, role34>
    /* 16 */  <role1, t11, role14 & role32, t17, role34>
    /* 17 */  <role1, t6, role14 & role32, t10, role34>
    /* 18 */  <role22, t20, TRUE, t15, role23>
    /* 19 */  <TRUE, t6, TRUE, t3, role23>
    /* 20 */  <role22, t2, TRUE, t13, role7>
    /* 21 */  <TRUE, t7, TRUE, t15, role21>
    /* 22 */  <role22, t3, TRUE, t18, role3>
    /* 23 */  <role22, t18, TRUE, t7, role3>
    /* 24 */  <TRUE, t14, TRUE, t11, role4>
    /* 25 */  <role22, t6, TRUE, t13, role28>
    /* 26 */  <role22, t6, TRUE, t5, role7>
    /* 27 */  <role22, t3, TRUE, t13, role21>
    /* 28 */  <role22, t5, TRUE, t10, role21>
    /* 29 */  <role22, t7, TRUE, t7, role21>
    /* 30 */  <role22, t15, TRUE, t15, role3>
    /* 31 */  <role22, t11, TRUE, t11, role3>
    /* 32 */  <role2, t1, NOT ~ role14, t5, role32>
    /* 33 */  <role2, t7, NOT ~ role14, t18, role32>
    /* 34 */  <role8, t12, role14, t7, role17>
    /* 35 */  <role8, t19, role17, t10, role16>
    /* 36 */  <role8, t7, role14, t12, role18>
    /* 37 */  <role8, t20, role14, t1, role18>
    /* 38 */  <TRUE, t3, role14, t5, role18>
    /* 39 */  <TRUE, t18, role32, t1, role18>
    /* 40 */  <role12, t17, role29, t20, role27>
    /* 41 */  <role12, t16, role14, t13, role27>
    /* 42 */  <role12, t3, role14, t14, role27>
    /* 43 */  <role12, t18, role14, t18, role27>
    /* 44 */  <role12, t19, role14, t19, role27>
    /* 45 */  <role12, t9, role14, t20, role27>
    /* 46 */  <role12, t2, role14, t4, role27>
    /* 47 */  <role12, t15, role14, t5, role27>
    /* 48 */  <role15, t14, NOT ~ role32, t1, role14>
    /* 49 */  <TRUE, t16, NOT ~ role32, t16, role14>
    /* 50 */  <role15, t10, NOT ~ role32, t19, role14>
    /* 51 */  <role15, t3, NOT ~ role32, t3, role14>
    /* 52 */  <TRUE, t17, role14, t19, role20>
    /* 53 */  <role19, t11, role14, t16, role20>
    /* 54 */  <TRUE, t6, role14, t16, role33>
    /* 55 */  <role22, t20, role24, t15, role13>
    /* 56 */  <TRUE, t13, role28, t3, role13>
    /* 57 */  <role22, t19, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t17, role24>
    /* 58 */  <role24, t11, role23 & NOT ~ role8 & NOT ~ role22 & NOT ~ role24, t14, role6>
    /* 59 */  <TRUE, t4, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t8, role8>
}

/* 
 * Number of Rules       : 36
 * Largest Precondition  : 4
 * Largest Role Schedule : 1
 * Startable Rules       : 21
 * Truly Startable Rules : 2
 */
CanDisable {
    /*  1 */  <role6, t11, TRUE, t10, role32>
    /*  2 */  <TRUE, t12, TRUE, t15, role32>
    /*  3 */  <role8, t18, TRUE, t16, role30>
    /*  4 */  <role8, t2, TRUE, t10, role17>
    /*  5 */  <role6, t5, TRUE, t19, role14>
    /*  6 */  <role19, t1, TRUE, t6, role20>
    /*  7 */  <role24, t11, TRUE, t13, role6>
    /*  8 */  <role1, t17, role14 & role32, t19, role34>
    /*  9 */  <role22, t3, TRUE, t11, role23>
    /* 10 */  <role22, t1, TRUE, t11, role7>
    /* 11 */  <TRUE, t11, TRUE, t13, role3>
    /* 12 */  <role22, t13, TRUE, t9, role23>
    /* 13 */  <role22, t4, TRUE, t11, role28>
    /* 14 */  <role22, t10, TRUE, t12, role7>
    /* 15 */  <role22, t4, TRUE, t12, role21>
    /* 16 */  <role22, t14, TRUE, t19, role21>
    /* 17 */  <role22, t12, TRUE, t17, role21>
    /* 18 */  <role22, t20, TRUE, t5, role3>
    /* 19 */  <role22, t17, TRUE, t2, role3>
    /* 20 */  <role22, t7, TRUE, t8, role3>
    /* 21 */  <role2, t17, NOT ~ role14, t12, role32>
    /* 22 */  <role8, t12, role14, t3, role30>
    /* 23 */  <role8, t6, role17, t1, role16>
    /* 24 */  <role8, t17, role17, t7, role16>
    /* 25 */  <TRUE, t5, role14, t17, role27>
    /* 26 */  <role12, t9, role32, t9, role27>
    /* 27 */  <role12, t8, role32, t12, role27>
    /* 28 */  <role15, t12, NOT ~ role32, t20, role14>
    /* 29 */  <role19, t14, role14, t9, role20>
    /* 30 */  <role22, t14, role24, t20, role13>
    /* 31 */  <TRUE, t3, role24, t10, role13>
    /* 32 */  <role22, t13, role23, t11, role13>
    /* 33 */  <role22, t17, role23, t12, role13>
    /* 34 */  <role22, t17, role28, t18, role13>
    /* 35 */  <role22, t16, role23 & NOT ~ role6 & NOT ~ role8 & NOT ~ role22, t1, role24>
    /* 36 */  <role6, t7, role23 & NOT ~ role6 & NOT ~ role22 & NOT ~ role24, t2, role8>
}