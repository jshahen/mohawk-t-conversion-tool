/* Generated On        : 2015/02/07 20:24:43.426
 * Generated With      : Mohawk Reverse Converter: ASAPTime NSA
 * Number of Roles     : 15
 * Number of Timeslots : 10
 * Number of Rules     : 287
 * 
 * Roles     : |Size=15| [role1, role2, role3, role4, role5, role6, role7, role8, role9, role10, role11, role12, role13, role14, role15]
 * Timeslots : |Size=10| [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t4, [role15]

Expected: UNKNOWN

/* 
 * Number of Rules       : 125
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 68
 * Truly Startable Rules : 16
 */
CanAssign {
    /*   1 */  <role1, t10, role12 & role3, t3, role15>
    /*   2 */  <role1, t5, role12 & role3, t6, role15>
    /*   3 */  <role1, t7, role12 & role3, t4, role15>
    /*   4 */  <role1, t9, role12 & role3, t9, role15>
    /*   5 */  <role1, t3, role12 & role3, t10, role15>
    /*   6 */  <role1, t1, role12 & role3, t5, role15>
    /*   7 */  <role3, t1, TRUE, t7, role14>
    /*   8 */  <role3, t2, TRUE, t10, role14>
    /*   9 */  <role3, t8, TRUE, t8, role14>
    /*  10 */  <role3, t3, TRUE, t1, role14>
    /*  11 */  <role3, t10, TRUE, t3, role14>
    /*  12 */  <role3, t7, TRUE, t9, role14>
    /*  13 */  <role3, t4, TRUE, t2, role14>
    /*  14 */  <role3, t6, TRUE, t4, role14>
    /*  15 */  <role5, t3, TRUE, t2, role4>
    /*  16 */  <TRUE, t7, TRUE, t9, role4>
    /*  17 */  <TRUE, t2, TRUE, t10, role4>
    /*  18 */  <TRUE, t10, TRUE, t3, role6>
    /*  19 */  <role5, t7, TRUE, t1, role6>
    /*  20 */  <role5, t5, TRUE, t9, role8>
    /*  21 */  <role5, t6, TRUE, t4, role8>
    /*  22 */  <role5, t10, TRUE, t1, role8>
    /*  23 */  <role5, t8, TRUE, t5, role8>
    /*  24 */  <role9, t8, TRUE, t1, role2>
    /*  25 */  <role9, t7, TRUE, t3, role2>
    /*  26 */  <TRUE, t1, TRUE, t6, role2>
    /*  27 */  <role9, t3, TRUE, t8, role2>
    /*  28 */  <TRUE, t4, TRUE, t3, role14>
    /*  29 */  <TRUE, t9, TRUE, t1, role14>
    /*  30 */  <role5, t7, TRUE, t1, role4>
    /*  31 */  <role2, t1, role12 & role3, t4, role15>
    /*  32 */  <role5, t9, TRUE, t5, role4>
    /*  33 */  <role5, t8, TRUE, t4, role4>
    /*  34 */  <role5, t6, TRUE, t4, role6>
    /*  35 */  <role2, t7, role12 & role3, t4, role15>
    /*  36 */  <role5, t5, TRUE, t6, role6>
    /*  37 */  <role5, t8, TRUE, t7, role6>
    /*  38 */  <TRUE, t2, TRUE, t8, role6>
    /*  39 */  <role5, t1, TRUE, t9, role6>
    /*  40 */  <role5, t10, TRUE, t5, role6>
    /*  41 */  <role5, t4, TRUE, t10, role6>
    /*  42 */  <role5, t3, TRUE, t9, role8>
    /*  43 */  <role5, t4, TRUE, t10, role8>
    /*  44 */  <role3, t7, role12 & role3, t4, role15>
    /*  45 */  <TRUE, t10, TRUE, t1, role8>
    /*  46 */  <role5, t8, TRUE, t7, role8>
    /*  47 */  <role9, t9, TRUE, t2, role2>
    /*  48 */  <role9, t1, TRUE, t10, role2>
    /*  49 */  <role9, t6, TRUE, t8, role2>
    /*  50 */  <role9, t5, TRUE, t1, role2>
    /*  51 */  <role9, t2, TRUE, t9, role2>
    /*  52 */  <role9, t3, TRUE, t3, role2>
    /*  53 */  <role1, t9, role12 & role3, t4, role15>
    /*  54 */  <role9, t8, TRUE, t4, role2>
    /*  55 */  <TRUE, t4, TRUE, t5, role2>
    /*  56 */  <role9, t7, TRUE, t6, role2>
    /*  57 */  <role3, t8, role3, t1, role13>
    /*  58 */  <role3, t2, role3, t4, role13>
    /*  59 */  <TRUE, t6, role3, t8, role13>
    /*  60 */  <role3, t5, role3, t3, role13>
    /*  61 */  <role3, t1, role3, t6, role13>
    /*  62 */  <role1, t6, role12 & role3, t4, role15>
    /*  63 */  <role3, t3, role3, t9, role13>
    /*  64 */  <role3, t4, role3, t2, role13>
    /*  65 */  <role3, t7, role3, t10, role13>
    /*  66 */  <TRUE, t9, role3, t5, role13>
    /*  67 */  <TRUE, t10, role3, t7, role13>
    /*  68 */  <TRUE, t9, role3, t3, role7>
    /*  69 */  <role6, t10, role3, t1, role7>
    /*  70 */  <role6, t7, role3, t10, role7>
    /*  71 */  <role6, t1, role3, t8, role7>
    /*  72 */  <role6, t4, role3, t9, role7>
    /*  73 */  <role6, t3, role3, t2, role7>
    /*  74 */  <role6, t8, role3, t4, role7>
    /*  75 */  <role5, t7, role12 & role3, t4, role15>
    /*  76 */  <role6, t9, role8, t7, role7>
    /*  77 */  <role6, t6, role8, t10, role7>
    /*  78 */  <TRUE, t4, role8, t9, role7>
    /*  79 */  <role6, t8, role8, t2, role7>
    /*  80 */  <role6, t10, role8, t1, role7>
    /*  81 */  <role6, t1, role8, t3, role7>
    /*  82 */  <role6, t2, role8, t4, role7>
    /*  83 */  <role6, t3, role8, t5, role7>
    /*  84 */  <role5, t2, NOT ~ role3, t9, role12>
    /*  85 */  <role1, t8, role12 & role3, t4, role15>
    /*  86 */  <role5, t8, NOT ~ role3, t10, role12>
    /*  87 */  <role5, t5, NOT ~ role3, t1, role12>
    /*  88 */  <role5, t9, NOT ~ role3, t2, role12>
    /*  89 */  <role5, t10, NOT ~ role3, t3, role12>
    /*  90 */  <role5, t6, NOT ~ role3, t4, role12>
    /*  91 */  <role5, t4, NOT ~ role3, t5, role12>
    /*  92 */  <TRUE, t1, NOT ~ role12, t6, role3>
    /*  93 */  <role5, t5, NOT ~ role12, t5, role3>
    /*  94 */  <TRUE, t3, NOT ~ role12, t9, role3>
    /*  95 */  <role5, t8, NOT ~ role12, t8, role3>
    /*  96 */  <role1, t8, role12 & role3, t4, role15>
    /*  97 */  <role5, t2, NOT ~ role12, t10, role3>
    /*  98 */  <role9, t8, role3 & NOT ~ role9, t5, role11>
    /*  99 */  <TRUE, t1, role3 & NOT ~ role9, t2, role11>
    /* 100 */  <role1, t6, role12 & role3, t4, role15>
    /* 101 */  <role9, t2, role3 & NOT ~ role9, t8, role11>
    /* 102 */  <role9, t3, role3 & NOT ~ role9, t6, role11>
    /* 103 */  <role9, t6, role3 & NOT ~ role9, t7, role11>
    /* 104 */  <role9, t4, role3 & NOT ~ role9, t9, role11>
    /* 105 */  <role12, t4, NOT ~ role11, t10, role9>
    /* 106 */  <TRUE, t9, NOT ~ role11, t7, role9>
    /* 107 */  <role12, t7, NOT ~ role11, t3, role9>
    /* 108 */  <role1, t3, role12 & role3, t4, role15>
    /* 109 */  <role12, t1, NOT ~ role11, t9, role9>
    /* 110 */  <TRUE, t2, NOT ~ role11, t6, role9>
    /* 111 */  <TRUE, t8, NOT ~ role11, t4, role9>
    /* 112 */  <TRUE, t3, NOT ~ role11, t5, role9>
    /* 113 */  <role12, t10, NOT ~ role11, t1, role9>
    /* 114 */  <role12, t5, NOT ~ role11, t8, role9>
    /* 115 */  <TRUE, t6, NOT ~ role11, t2, role9>
    /* 116 */  <role14, t8, role9, t9, role10>
    /* 117 */  <role14, t2, role9, t10, role10>
    /* 118 */  <role14, t5, role9, t2, role10>
    /* 119 */  <role14, t6, role9, t5, role10>
    /* 120 */  <TRUE, t9, role9, t6, role10>
    /* 121 */  <role14, t7, role9, t7, role10>
    /* 122 */  <role14, t10, role9, t3, role10>
    /* 123 */  <role14, t1, role9, t8, role10>
    /* 124 */  <role1, t1, role12 & role3, t4, role15>
    /* 125 */  <role14, t3, role9, t1, role10>
}

/* 
 * Number of Rules       : 87
 * Largest Precondition  : 0
 * Largest Role Schedule : 1
 * Startable Rules       : 87
 * Truly Startable Rules : 20
 */
CanRevoke {
    /*  1 */  <role3, t10, TRUE, t6, role14>
    /*  2 */  <role3, t9, TRUE, t5, role14>
    /*  3 */  <TRUE, t1, TRUE, t4, role14>
    /*  4 */  <TRUE, t7, TRUE, t9, role14>
    /*  5 */  <TRUE, t4, TRUE, t2, role14>
    /*  6 */  <role3, t9, TRUE, t8, role13>
    /*  7 */  <TRUE, t4, TRUE, t10, role13>
    /*  8 */  <role3, t8, TRUE, t3, role13>
    /*  9 */  <role3, t10, TRUE, t2, role13>
    /* 10 */  <role6, t4, TRUE, t8, role7>
    /* 11 */  <role6, t5, TRUE, t9, role7>
    /* 12 */  <role6, t7, TRUE, t2, role7>
    /* 13 */  <role6, t1, TRUE, t7, role7>
    /* 14 */  <role6, t3, TRUE, t5, role7>
    /* 15 */  <role6, t2, TRUE, t10, role7>
    /* 16 */  <TRUE, t6, TRUE, t3, role7>
    /* 17 */  <role6, t8, TRUE, t1, role7>
    /* 18 */  <role6, t9, TRUE, t6, role7>
    /* 19 */  <role6, t7, TRUE, t5, role7>
    /* 20 */  <role6, t10, TRUE, t1, role7>
    /* 21 */  <TRUE, t1, TRUE, t7, role7>
    /* 22 */  <TRUE, t2, TRUE, t8, role7>
    /* 23 */  <role6, t3, TRUE, t3, role7>
    /* 24 */  <role6, t6, TRUE, t2, role7>
    /* 25 */  <TRUE, t5, TRUE, t8, role4>
    /* 26 */  <role5, t1, TRUE, t9, role4>
    /* 27 */  <role5, t3, TRUE, t3, role4>
    /* 28 */  <role5, t4, TRUE, t1, role4>
    /* 29 */  <role5, t10, TRUE, t5, role4>
    /* 30 */  <TRUE, t9, TRUE, t2, role4>
    /* 31 */  <role5, t7, TRUE, t6, role4>
    /* 32 */  <TRUE, t7, TRUE, t9, role6>
    /* 33 */  <role5, t4, TRUE, t10, role6>
    /* 34 */  <role5, t9, TRUE, t4, role6>
    /* 35 */  <role5, t6, TRUE, t1, role6>
    /* 36 */  <role5, t8, TRUE, t5, role6>
    /* 37 */  <role5, t1, TRUE, t3, role6>
    /* 38 */  <role5, t10, TRUE, t6, role6>
    /* 39 */  <TRUE, t2, TRUE, t7, role6>
    /* 40 */  <TRUE, t3, TRUE, t8, role6>
    /* 41 */  <role5, t5, TRUE, t2, role6>
    /* 42 */  <role5, t3, TRUE, t4, role12>
    /* 43 */  <role5, t8, TRUE, t10, role12>
    /* 44 */  <role5, t5, TRUE, t3, role12>
    /* 45 */  <role5, t2, TRUE, t1, role12>
    /* 46 */  <TRUE, t2, TRUE, t1, role8>
    /* 47 */  <role5, t7, TRUE, t7, role8>
    /* 48 */  <role5, t3, TRUE, t9, role8>
    /* 49 */  <role5, t9, TRUE, t4, role3>
    /* 50 */  <role5, t1, TRUE, t5, role3>
    /* 51 */  <role5, t7, TRUE, t2, role3>
    /* 52 */  <role5, t3, TRUE, t6, role3>
    /* 53 */  <role5, t8, TRUE, t7, role3>
    /* 54 */  <role5, t6, TRUE, t8, role3>
    /* 55 */  <role9, t5, TRUE, t3, role2>
    /* 56 */  <role9, t8, TRUE, t10, role2>
    /* 57 */  <role9, t9, TRUE, t6, role2>
    /* 58 */  <role9, t3, TRUE, t2, role2>
    /* 59 */  <role9, t2, TRUE, t1, role2>
    /* 60 */  <TRUE, t4, TRUE, t7, role2>
    /* 61 */  <role9, t10, TRUE, t4, role2>
    /* 62 */  <role9, t1, TRUE, t8, role2>
    /* 63 */  <TRUE, t6, TRUE, t5, role2>
    /* 64 */  <TRUE, t7, TRUE, t9, role2>
    /* 65 */  <role9, t2, TRUE, t5, role11>
    /* 66 */  <role9, t9, TRUE, t6, role11>
    /* 67 */  <role9, t10, TRUE, t7, role11>
    /* 68 */  <role9, t1, TRUE, t8, role11>
    /* 69 */  <TRUE, t8, TRUE, t9, role11>
    /* 70 */  <role9, t5, TRUE, t10, role11>
    /* 71 */  <role9, t7, TRUE, t3, role11>
    /* 72 */  <role3, t3, TRUE, t1, role9>
    /* 73 */  <TRUE, t5, TRUE, t6, role9>
    /* 74 */  <role3, t4, TRUE, t2, role9>
    /* 75 */  <role3, t6, TRUE, t5, role9>
    /* 76 */  <role3, t7, TRUE, t3, role9>
    /* 77 */  <TRUE, t1, TRUE, t4, role9>
    /* 78 */  <role3, t9, TRUE, t9, role9>
    /* 79 */  <role14, t9, TRUE, t8, role10>
    /* 80 */  <role14, t4, TRUE, t4, role10>
    /* 81 */  <role14, t5, TRUE, t10, role10>
    /* 82 */  <role14, t10, TRUE, t7, role10>
    /* 83 */  <role14, t6, TRUE, t2, role10>
    /* 84 */  <role14, t1, TRUE, t3, role10>
    /* 85 */  <TRUE, t7, TRUE, t9, role10>
    /* 86 */  <role14, t8, TRUE, t1, role10>
    /* 87 */  <role14, t2, TRUE, t5, role10>
}

/* 
 * Number of Rules       : 52
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 45
 * Truly Startable Rules : 13
 */
CanEnable {
    /*  1 */  <role3, t10, TRUE, t6, role14>
    /*  2 */  <TRUE, t7, TRUE, t9, role14>
    /*  3 */  <TRUE, t4, TRUE, t10, role13>
    /*  4 */  <TRUE, t9, TRUE, t1, role12>
    /*  5 */  <role5, t6, TRUE, t4, role2>
    /*  6 */  <TRUE, t2, TRUE, t8, role1>
    /*  7 */  <role5, t10, TRUE, t5, role2>
    /*  8 */  <role5, t4, TRUE, t10, role3>
    /*  9 */  <role3, t10, TRUE, t2, role13>
    /* 10 */  <role6, t5, TRUE, t9, role7>
    /* 11 */  <role6, t1, TRUE, t7, role7>
    /* 12 */  <TRUE, t6, TRUE, t3, role7>
    /* 13 */  <role6, t6, TRUE, t2, role7>
    /* 14 */  <role5, t9, TRUE, t4, role6>
    /* 15 */  <role5, t3, TRUE, t4, role12>
    /* 16 */  <role5, t1, TRUE, t5, role3>
    /* 17 */  <role9, t5, TRUE, t3, role2>
    /* 18 */  <role9, t8, TRUE, t10, role2>
    /* 19 */  <role9, t1, TRUE, t8, role2>
    /* 20 */  <role9, t10, TRUE, t7, role11>
    /* 21 */  <TRUE, t5, TRUE, t6, role9>
    /* 22 */  <role3, t4, TRUE, t2, role9>
    /* 23 */  <role3, t9, TRUE, t9, role9>
    /* 24 */  <role14, t6, TRUE, t2, role10>
    /* 25 */  <role1, t5, role12 & role3, t6, role15>
    /* 26 */  <role3, t4, TRUE, t2, role14>
    /* 27 */  <role3, t6, TRUE, t4, role14>
    /* 28 */  <role5, t5, TRUE, t9, role8>
    /* 29 */  <role5, t10, TRUE, t1, role8>
    /* 30 */  <role9, t8, TRUE, t1, role2>
    /* 31 */  <role9, t7, TRUE, t3, role2>
    /* 32 */  <TRUE, t1, TRUE, t6, role2>
    /* 33 */  <role9, t3, TRUE, t8, role2>
    /* 34 */  <TRUE, t7, TRUE, t9, role15>
    /* 35 */  <TRUE, t4, TRUE, t10, role11>
    /* 36 */  <TRUE, t4, TRUE, t3, role14>
    /* 37 */  <TRUE, t9, TRUE, t1, role14>
    /* 38 */  <role5, t6, TRUE, t4, role6>
    /* 39 */  <TRUE, t2, TRUE, t8, role6>
    /* 40 */  <role5, t10, TRUE, t5, role6>
    /* 41 */  <role5, t4, TRUE, t10, role6>
    /* 42 */  <TRUE, t4, TRUE, t5, role2>
    /* 43 */  <role9, t7, TRUE, t6, role2>
    /* 44 */  <role3, t2, role3, t4, role13>
    /* 45 */  <role6, t8, role8, t2, role7>
    /* 46 */  <role5, t2, NOT ~ role12, t10, role3>
    /* 47 */  <role9, t3, role3 & NOT ~ role9, t6, role11>
    /* 48 */  <role9, t4, role3 & NOT ~ role9, t9, role11>
    /* 49 */  <role12, t4, NOT ~ role11, t10, role9>
    /* 50 */  <role12, t7, NOT ~ role11, t3, role9>
    /* 51 */  <role14, t6, role9, t5, role10>
    /* 52 */  <role14, t1, role9, t8, role10>
}

/* 
 * Number of Rules       : 23
 * Largest Precondition  : 2
 * Largest Role Schedule : 1
 * Startable Rules       : 18
 * Truly Startable Rules : 2
 */
CanDisable {
    /*  1 */  <TRUE, t4, TRUE, t2, role14>
    /*  2 */  <role5, t7, TRUE, t1, role9>
    /*  3 */  <role6, t3, TRUE, t3, role7>
    /*  4 */  <role5, t1, TRUE, t3, role6>
    /*  5 */  <role5, t8, TRUE, t10, role12>
    /*  6 */  <role5, t7, TRUE, t2, role3>
    /*  7 */  <role5, t3, TRUE, t6, role3>
    /*  8 */  <role3, t7, TRUE, t3, role9>
    /*  9 */  <role14, t8, TRUE, t1, role10>
    /* 10 */  <role3, t10, TRUE, t3, role14>
    /* 11 */  <TRUE, t4, TRUE, t2, role12>
    /* 12 */  <role5, t7, TRUE, t1, role4>
    /* 13 */  <role9, t1, TRUE, t10, role2>
    /* 14 */  <role9, t5, TRUE, t1, role2>
    /* 15 */  <role9, t2, TRUE, t9, role2>
    /* 16 */  <TRUE, t9, role3, t5, role13>
    /* 17 */  <role6, t6, role8, t10, role7>
    /* 18 */  <role6, t3, role8, t5, role7>
    /* 19 */  <role5, t8, NOT ~ role3, t10, role12>
    /* 20 */  <role9, t2, role3 & NOT ~ role9, t8, role11>
    /* 21 */  <role12, t10, NOT ~ role11, t1, role9>
    /* 22 */  <role12, t5, NOT ~ role11, t8, role9>
    /* 23 */  <role14, t2, role9, t10, role10>
}