Query: t0, [role1]
Expected: REACHABLE
CanAssign {
    <TRUE, t2, TRUE, [t1], role1>
    <role1, t1, TRUE, [t0], role2>
}

CanRevoke {}

CanEnable {}

CanDisable {}