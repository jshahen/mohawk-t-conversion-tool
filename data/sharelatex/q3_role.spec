/* 
 * Question 3: 
 * 
 * Roles: role1, role2
 * Time-Slots: t0, t1
 * 
 * if any of the rules are triggered it will show that some roles are allowed to be activated without a can assign
 */
 
Query: t0,[role2]
Expected: TRUE

CanAssign {
    /* <Admin role, 
     * starting timeslot - end timeslot for admin role, 
     * role preconditions, 
     * [time slot rule gives role to user for, another time slot (optional)...],
     * role to give to the user>
     */
    <TRUE,t0,role1,t0,role2>           //(1)
    <TRUE,t0,role2,t0,role1>           //(2)
    
    /* Trying to include other time-slots to test with TRule */
    <TRUE,t1,role1,t0,role2>           //(3) 
    <TRUE,t1,role2,t0,role1>           //(4) 
}

CanRevoke {}

CanEnable {}

CanDisable {}
