/* Generated On        : 2015/01/23 22:00:02.096
 * Generated With      : Mohawk Converter
 * Number of Roles     : 4
 * Number of Timeslots : 4
 * Number of Rules     : 3
 * 
 * Roles     : [role0, role1, role2, role3]
 * Timeslots : [t0, t1, t2, t5]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t0, [role1, role0, role2]

Expected: UNKNOWN

/* 
 * Number of Rules       : 3
 * Largest Precondition  : 3
 * Largest Role Schedule : 2
 * Startable Rules       : 2
 * Truly Startable Rules : 0
 */
CanAssign {
    /* 1 */  <role0, t0-t5, TRUE, [t1, t5], role1>
    /* 2 */  <role0, t0, role1 & NOT ~ role2 & role3, [t1, t2], role2>
    /* 3 */  <role0, t0, NOT ~ role3, t1, role3>
}

CanRevoke { }

CanEnable { }

CanDisable { }