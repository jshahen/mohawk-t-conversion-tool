/* Generated On        : 2015/01/23 21:53:32.640
 * Generated With      : Mohawk Converter
 * Number of Roles     : 6
 * Number of Timeslots : 4
 * Number of Rules     : 7
 * 
 * Roles     : [badguy, contractor, employee, manager, officer, person]
 * Timeslots : [t0, t1, t2, t5]
 * 
 * 
 * TESTCASE COMMENTS:
 * 
 */

Query: t0, [officer, manager, employee]

Expected: UNKNOWN

/* 
 * Number of Rules       : 3
 * Largest Precondition  : 3
 * Largest Role Schedule : 2
 * Startable Rules       : 1
 * Truly Startable Rules : 0
 */
CanAssign {
    /* 1 */  <manager, t0-t5, TRUE, [t1, t1], employee>
    /* 2 */  <officer, t0, employee & NOT ~ manager & NOT ~ badguy, [t1, t2], contractor>
    /* 3 */  <TRUE, t0, employee, [t1, t2], person>
}

CanRevoke { }

/* 
 * Number of Rules       : 2
 * Largest Precondition  : 3
 * Largest Role Schedule : 2
 * Startable Rules       : 1
 * Truly Startable Rules : 0
 */
CanEnable {
    /* 1 */  <manager, t0-t5, TRUE, [t1, t1], employee>
    /* 2 */  <officer, t0, employee & NOT ~ manager & NOT ~ badguy, [t1, t2], contractor>
}

/* 
 * Number of Rules       : 2
 * Largest Precondition  : 3
 * Largest Role Schedule : 2
 * Startable Rules       : 1
 * Truly Startable Rules : 0
 */
CanDisable {
    /* 1 */  <manager, t0-t5, TRUE, [t1, t1], employee>
    /* 2 */  <officer, t0, employee & NOT ~ manager & NOT ~ badguy, [t1, t2], contractor>
}