/* Taken from sharelatex's "Working Files/Mohawk Examples/old-simple-tests.txt"
 * 
 * Converted simple_test4.txt into the new format
 */
Query: t1,[role2]
Expected: FALSE // {TRUE, FALSE, UNKNOWN}

CanAssign {
    /* <Admin role, 
     * starting timeslot - end timeslot for admin role, 
     * role preconditions, 
     * [time slot rule gives role to user for, another time slot (optional)...],
     * role to give to the user>
     */
    <role0,t0,TRUE,t1,role0>		        // (5)
    <role0,t1,role0,t1,role1>		        // (6)
    <role0,t1,role1 & NOT role0,t1,role2>   // (7)
}

CanRevoke {
	<TRUE,t0,TRUE,t0,role1>		            // (1)
    <TRUE,t1,TRUE,t0,role1>		            // (2)
    <TRUE,t1,TRUE,t0,role1>		            // (3)
    <TRUE,t1,TRUE,t1,role1>		            // (4)
}

CanEnable {
	<role0,t0,TRUE,t1,role0>		        // (8)
}

CanDisable {
	<role0,t1,TRUE,t1,role0>		        // (9)
}
